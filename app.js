const { ApolloServer } = require('apollo-server');
const cors = require('cors')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/ktb_accress_control');

// call models mongoose
require("./models/users")
require("./models/devices")

// typeDefs
const typeDefs = require("./src/typeDefs")

// resolvers
const resolvers = require("./src/resolvers")

const server = new ApolloServer({ 
  typeDefs,
  resolvers,  
  context: ({ req }) => {
    const token = req.headers.authorization || '';
    const user = token;
    return { user };
  },
  playground: {
    settings: {
      'editor.theme': 'dark',
    },
  },
});

cors(server)

server.listen(6500).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});




// var crypto = require('crypto');
// var salt = crypto.randomBytes(16).toString('hex');
// console.log("this is salt: " + salt)