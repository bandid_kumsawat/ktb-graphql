const { gql } = require('apollo-server');

module.exports = typeDefs = gql`
  scalar Date

  type User {
    updatedAt: Date,
    createdAt: Date,
    hash: String,
    salt: String,
    password: String,
    type: String,
    email: String,
    username: String,
  }

  type time_open_door {
    start_time: Date, 
      end_time: Date
  }

  type Device {
    id: String,
    point_name: String,
    time_open_door: time_open_door,
    tablets: Boolean,
    iot: Boolean,
    sensor: Boolean,
    break_glass: Boolean,
    timestamp: Date
  }

  type Token {
    token: String
  }

  type Query {
    Login(
      username: String!, 
      password: String!,
    ) : Token!,
    FineDevice: [Device]
  }


`;

// export const typeDefs = gql`
// type Summary2 {
//   name: String,
//   surname: String,
//   date: String,
// }

// type Query {
//   Summary2: [Summary2],
//   SummaryByTime(start: String!, stop: String!): [Summary2]!
// }

// type Mutation {
//   createSummary2(name: String!, surname: String!) : Summary2!
// }


// type User {
//   updatedAt: String,
//   createdAt: String,
//   hash: String,
//   salt: String,
//   password: String,
//   type: String,
//   email: String,
//   username: String,
// }

// type Authentication {
//   Login(
//     username: String!, 
//     password: String!,
//   ) : [User]!,
//   Register(
//     username: String!, 
//     password: String!,
//     email: String!, 
//   ) : [User]!,
// }



// `;