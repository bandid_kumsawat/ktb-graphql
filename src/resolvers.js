const mongoose = require("mongoose")
var users = mongoose.model("users")
var devices = mongoose.model("devices")
var jwt = require('jsonwebtoken');

module.exports = resolvers = {
  Query: {
    Login: async (_, user) => {
      var query = {
        username: user.username,
        password: user.password,
      }
      const result = await users.find( query )
      if (result.length === 1){
        var today = new Date();
        var exp = new Date(today);
        exp.setDate(today.getDate() + 5);
      
        var token_ = jwt.sign({
          user: result[0],
          exp: parseInt(exp.getTime() / 1000),
        }, "MOCKAPI");
  
        var token = {
          token : token_
        }
        return token
      }else if (result.length === 0){
        var token = {
          token : ""
        }
        return token
      }
    },
    FineDevice: async () => devices.find({})
  },
};


// module.exports = resolvers = {
//   Query: {
//     Summary2: () => summary2.find(),
//     SummaryByTime: async (_, QueryTime, { user }) => {
//       console.log(_)
//       console.log(user)
//       var qry = {
//         date: {
//           $gte: new Date(QueryTime.start),
//           $lte: new Date(QueryTime.stop)
//         }
//       }
//       return summary2.find(qry).sort({ date: -1 })
//     }
//   },
//   Mutation: {
//     createSummary2: async (_, newData) => {
//       var newObj = {}
//       newObj.name = newData.name
//       newObj.surname = newData.surname
//       newObj.date = new Date().toISOString()
//       const newSummary2 = new summary2(newObj)
//       await newSummary2.save()
//       return newSummary2
//     }
//   }
// };