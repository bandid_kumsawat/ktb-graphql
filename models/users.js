var mongoose = require('mongoose');

var Users = new mongoose.Schema({
  updatedAt: Date,
  createdAt: Date,
  hash: String,
  salt: String,
  password: String,
  type: String,
  email: String,
  username: String,
});
mongoose.model('users', Users);

