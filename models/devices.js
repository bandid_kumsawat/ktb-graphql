var mongoose = require('mongoose');

var Devices = new mongoose.Schema({
  id: String,
  point_name: String,
  time_open_door: Object,
  tablets: Boolean,
  iot: Boolean,
  sensor: Boolean,
  break_glass: Boolean,
  timestamp: Date
});
mongoose.model('devices', Devices);

